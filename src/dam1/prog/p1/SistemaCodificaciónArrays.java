package dam1.prog.p1;
/*
 * Se pretende realizar un programa que sirva para codificar el alfabeto. Utilizaremos dos arrays, uno que contenga
 * todas las letras y otro que contenga un carácter que se corresponda con una letra. Esta relación vendrá dada por el
 * índice.
 *
 * El objetivo es poder introducir frases por teclado para generar una palabra codificada según el caso en que nos
 * encontramos, mostrarla, descodificarla de nuevo según el array y volverla a mostrar en su estado original.
 *
 * -Caso 1: Los índices de los caracteres y los códigos tienen la siguiente relación, a[i]= b[i], es decir, el a[0] se
 * corresponde con b[0], a[1] = b[1]...
 * -Caso 2: Los índices de los caracteres y los códigos tienen la siguiente relación a[i] = b[i + 1]. Recordad que los
 * arrays tiene tamaño fijo y que no podemos salir de sus límites, con lo que habrá que manipular esta situación de
 * alguna manera.
 *
 * Se utilizarán los siguientes arrays para completar el programa.
 * char[] alfabeto = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'ñ', 'o', 'p', 'q', 'r',
 * 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
 * char[] caracteres = {'@','#','$','%','&','/','(',')','=','?','¿','*','+',']','[','{','_','-','.',':','.',':','<',
 * '>','/','a','o'};
 */

import java.util.Scanner;

/**
 * @author Beatriz
 */
public class SistemaCodificaciónArrays {

    //Creamos los arrays que vamos a utilizar de guia para descifrar los datos del usuario//
    static char[] ALFABETO = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
            'm', 'n', 'ñ', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    static char[] CARACTERES = {'@', '#', '$', '%', '&', '/', '(', ')', '=', '?', '¿', '*', '+', ']',
            '[', '{', '_', '-', '.', ':', '.', ':', '<', '>', '/', 'a', 'o'};

    public static void main(String[] args) {
        //Creamos el objeto Scanner//
        Scanner key = new Scanner(System.in);
        boolean salir = false;
        int opcion;
        //creamos un menu para poder acceder a cualquiera de los dos casos//
        while (!salir) {
            System.out.println("1. Caso 1");
            System.out.println("2. Caso 2");
            System.out.println("3. Salir");
            opcion = key.nextInt();
            switch (opcion) {
                case 1:
                    caso1();
                    break;
                case 2:
                    caso2();
                    break;
                case 3:
                    salir = true;
                    break;
                default:
                    System.out.println("Opción no válida");

            }
        }
    }

    //Creamos el metodo caso1 para llamarlo en el menu//
    public static void caso1() {
        //Creamos el objeto Scanner//
        Scanner key = new Scanner(System.in);
        //Pedimos los datos al usuario//
        System.out.println("Introduce la frase a codificar");
        String frase = key.nextLine();

        //Inicializamos otra variable//
        char[] fraseUsuario = frase.toCharArray();
        char[] fraseCodificada = new char[fraseUsuario.length];

        //Mira letra por letra y compara los dos arrays para hayar el mismo indice y codificar la frase//
        for (int i = 0; i < fraseUsuario.length; i++) {
            for (int j = 0; j < ALFABETO.length; j++) {
                if (fraseUsuario[i] == ALFABETO[j]) {
                    fraseCodificada[i] = CARACTERES[j];
                }
            }
        }
        //muestra la frase codificada//
        for (int i = 0; i < fraseCodificada.length; i++) {
            System.out.print(fraseCodificada[i]);
        }
        System.out.println();
        //descodifica la frase para comprobar que su codificacion es correcta//
        char[] fraseFinal = new char[fraseCodificada.length];
        for (int i = 0; i < fraseCodificada.length; i++) {
            for (int j = 0; j < CARACTERES.length; j++) {
                if (fraseCodificada[i] == CARACTERES[j]) {
                    fraseFinal[i] = ALFABETO[j];
                }
            }
        }
        //mostramos la descodificacion de la frase//
        for (int i = 0; i < fraseFinal.length; i++) {
            System.out.print(fraseFinal[i]);
        }
        System.out.println();
    }

    public static void caso2() {
        //Creamos el objeto Scanner//
        Scanner key = new Scanner(System.in);
        //Cambiamos las posiciones del alfabeto para que coincidan con lo que nos han pedido//
        for (int i = 0; i < ALFABETO.length - 1; i++) {
            char aux = ALFABETO[i + 1];
            ALFABETO[i + 1] = ALFABETO[0];
            ALFABETO[0] = aux;
        }

        //Pedimos los datos al usuario//
        System.out.println("Introduce la frase a codificar");
        String frase = key.nextLine();

        //Inicializamos otra variable//
        char[] fraseUsuario = frase.toCharArray();
        char[] fraseCodificada = new char[fraseUsuario.length];

        //Cambiamos los datos//
        for (int i = 0; i < fraseUsuario.length; i++) {
            for (int j = 0; j < ALFABETO.length; j++) {
                if (fraseUsuario[i] == ALFABETO[j]) {
                    fraseCodificada[i] = CARACTERES[j];
                }
            }
        }

        //Mostramos la frase codificada//
        for (int i = 0; i < fraseCodificada.length; i++) {
            System.out.print(fraseCodificada[i]);
        }

        System.out.println();

        char[] fraseFinal = new char[fraseCodificada.length];

        //Volvemos a mostrar la frase sin codificar//
        for (int i = 0; i < fraseCodificada.length; i++) {
            for (int j = 0; j < CARACTERES.length; j++) {
                if (fraseCodificada[i] == CARACTERES[j]) {
                    fraseFinal[i] = ALFABETO[j];
                }
            }
        }

        //Mostramos la frase original//
        for (int i = 0; i < fraseFinal.length; i++) {
            System.out.print(fraseFinal[i]);
        }
        System.out.println();
    }
}
